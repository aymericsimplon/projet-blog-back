package com.blog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blog.projetblog.entity.Commentaire;

@Repository
public class CommentaireRepository {

    @Autowired
    private DataSource dataSource;

    public List<Commentaire> findAll() {
        List<Commentaire> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentaires");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(new Commentaire(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getDate("date"),
                        result.getString("content")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;

    }

    public Commentaire findById(int id) {

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentaires WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Commentaire(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getDate("date"),
                        result.getString("content"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean persist(Commentaire commentaire) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO commentaires (name,date,content) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, commentaire.getName());
            stmt.setDate(1, commentaire.getDatetime());
            stmt.setString(1, commentaire.getContent());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                commentaire.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean update(Commentaire commentaire) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE commentaires SET name = ? WHERE id = ?");
            stmt.setString(1, commentaire.getName());
            stmt.setInt(4, commentaire.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commentaires WHERE id=?");
            stmt.setInt(1, id);

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
}
