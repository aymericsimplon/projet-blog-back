package com.blog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blog.projetblog.entity.Articles;

@Repository
public class ArticleRepository {

    @Autowired
    private DataSource dataSource;

    public List<Articles> findAll() {
        List<Articles> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM articles ");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(new Articles(
                        result.getInt("id"),
                        result.getString("title"),
                        result.getString("url"),
                        result.getString("content")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;

    }

    public List<Articles> findAllCate(int id_categories) {
        List<Articles> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM articles WHERE  id_catégories=?");
            stmt.setInt(1, id_categories);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(new Articles(
                        result.getInt("id"),
                        result.getString("title"),
                        result.getString("url"),
                        result.getString("content")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;

    }


    public Articles findById(int id) {

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM articles WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Articles(
                        result.getInt("id"),
                        result.getString("title"),
                        result.getString("url"),
                        result.getString("content"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean persist(Articles articles) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO articles (title,url,content) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, articles.getTitle());
            stmt.setString(2, articles.getUrl());
            stmt.setString(3, articles.getContent());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                articles.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean update(Articles articles) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE articles SET title =?, url =?, content =? WHERE id = ?");
            stmt.setString(1, articles.getTitle());
            stmt.setString(2, articles.getUrl());
            stmt.setString(3, articles.getContent());
            stmt.setInt(4, articles.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM articles WHERE id=?");
            stmt.setInt(1, id);

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

}