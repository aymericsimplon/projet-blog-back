package com.blog.projetblog.entity;

public class Articles {

    private Integer id;
    private String title;
    private String url;
    private String content;
    
    
    
    public Articles() {
    }
    
    public Articles(String title, String url, String content) {
        this.title = title;
        this.url = url;
        this.content = content;
    }
    
    public Articles(Integer id, String title, String url, String content) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.content = content;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }



}