package com.blog.projetblog.entity;

import java.sql.Date;

public class Commentaire {

    private Integer id;
    private String name;
    private Date datetime;
    private String content;
    
    public Commentaire() {
    }

    public Commentaire(String name, Date datetime, String content) {
        this.name = name;
        this.datetime = datetime;
        this.content = content;
    }

    public Commentaire(Integer id, String name, Date datetime, String content) {
        this.id = id;
        this.name = name;
        this.datetime = datetime;
        this.content = content;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getDatetime() {
        return datetime;
    }
    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    
}
