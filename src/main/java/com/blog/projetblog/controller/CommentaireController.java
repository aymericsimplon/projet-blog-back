package com.blog.projetblog.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.blog.projetblog.entity.Commentaire;
import com.blog.projetblog.repository.CommentaireRepository;

import jakarta.validation.Valid;



@RestController
@RequestMapping ("/api/commentaire")
public class CommentaireController {

    @Autowired
    private CommentaireRepository commentaireRepo;
    
    @GetMapping
    public List<Commentaire> getAllCommentaire() {
        return commentaireRepo.findAll();
    }
    @GetMapping("/{id}")
    public Commentaire one(@PathVariable int id) {
        Commentaire commentaire = commentaireRepo.findById(id);
        if (commentaire == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return commentaire;
}

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commentaire add(@Valid @RequestBody Commentaire commentaire) {
        commentaireRepo.persist(commentaire);
        return commentaire;
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        commentaireRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Commentaire replace (@PathVariable int id, @Valid @RequestBody Commentaire commentaire) {
        one(id);
        commentaire.setId(id);
        commentaireRepo.update(commentaire);
        return commentaire;
    }
}
