package com.blog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.blog.projetblog.entity.Categorie;
import com.blog.projetblog.repository.CategorieRepository;

import jakarta.validation.Valid;


@RestController
@RequestMapping("/api/categorie")
public class CategorieController {

    @Autowired
    private CategorieRepository categorieRepo;

    @GetMapping
    public List<Categorie> getAllCategorie() {
        return categorieRepo.findAll();
    }

    @GetMapping("/{id}")
    public Categorie one(@PathVariable int id) {
        Categorie categorie = categorieRepo.findById(id);
        if (categorie == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return categorie;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie add(@Valid @RequestBody Categorie categorie) {
        categorieRepo.persist(categorie);
        return categorie;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        categorieRepo.delete(id);
    }

    @PutMapping("/{id}")
    public boolean replace (@PathVariable int id, @Valid @RequestBody Categorie categorie) {
        one(id);
        categorie.setId(id);
    return categorieRepo.update(categorie);
    
}

}
