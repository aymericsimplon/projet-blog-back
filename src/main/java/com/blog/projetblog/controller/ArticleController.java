package com.blog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.blog.projetblog.entity.Articles;
import com.blog.projetblog.repository.ArticleRepository;

import jakarta.validation.Valid;



@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepo;

    @GetMapping("/categorie/{id}")
    public List<Articles> getArticlesCate(@PathVariable int id)  {
        return articleRepo.findAllCate(id);
    }

    @GetMapping
    public List<Articles> getAllArticles() {
        return articleRepo.findAll();
    }

    @GetMapping("/{id}")
    public Articles one(@PathVariable int id) {
        Articles article = articleRepo.findById(id);
        if (article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return article;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Articles add(@Valid @RequestBody Articles article) {
        articleRepo.persist(article);
        return article;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        articleRepo.delete(id);
    }

    @PutMapping("/{id}")
    public boolean replace(@PathVariable int id, @Valid @RequestBody Articles article) {
        one(id);
        article.setId(id);
        return articleRepo.update(article);
    }

}