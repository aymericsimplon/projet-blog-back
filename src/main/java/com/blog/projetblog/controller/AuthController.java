package com.blog.projetblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.blog.projetblog.entity.User;
import com.blog.projetblog.repository.UserRepository;


    @RestController
public class AuthController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepo;

    @GetMapping("/api/account")
    public User getUser(@AuthenticationPrincipal User user){
        return user;
    }
    @GetMapping("/api/protected")
    public String secretMessage(@AuthenticationPrincipal User user){
        System.out.println(user.getEmail());
        return "it is secret";
    }

    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@RequestBody User user) {

        if(userRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already Exist");
        }

        user.setRole("ROLE_USER");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepo.persist(user);

        return user;
    }
    
}

