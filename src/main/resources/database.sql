-- Active: 1709718790430@@127.0.0.1@3306@Blog

DROP TABLE IF EXISTS  commentaires;
DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS catégories;



CREATE TABLE catégories (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (350)
);

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(50) NOT NULL
    
);
CREATE TABLE articles (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR (350),
    url VARCHAR (350),
    content TEXT,
    id_user INT ,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    id_catégories INT,
    Foreign Key (id_catégories) REFERENCES catégories(id) ON DELETE CASCADE
    );


CREATE TABLE commentaires (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (350),
    date DATETIME,
    content TEXT,
    id_articles INT,
    Foreign Key (id_articles) REFERENCES articles(id) ON DELETE CASCADE
);





INSERT INTO catégories (name) VALUES 
('Actualité'),
('Combattants'),
('Champions');
INSERT INTO user (email,password,role) VALUES 
('test@test.com','$2a$12$MZh4.klAgZqlGMFHOZp3UOy37AvFwqPKAdeIX6USIJYYmo3FPPtlO', 'ROLE_USER'),
('admin@test.com','$2a$12$MZh4.klAgZqlGMFHOZp3UOy37AvFwqPKAdeIX6USIJYYmo3FPPtlO', 'ROLE_ADMIN');



INSERT INTO articles (title, url, content, id_catégories , id_user) VALUES
('Cyril Gane, la FRAUDE','https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Ciryl_Gane_en_visite_au_centre_d%27entra%C3%AEnement_du_MHSC_%28cropped%29.jpg/640px-Ciryl_Gane_en_visite_au_centre_d%27entra%C3%AEnement_du_MHSC_%28cropped%29.jpg', 'Du jamais vu, il y a un an Cyril se faisait soumettre en moins d une minute contre Jones qui revenait de trois années d absences',1, 2),
('Doumbe vs Baki un sujet qui PIQUE','https://images.ladepeche.fr/api/v1/images/view/65eae4e48e49c715501491d3/large/image.jpg?v=1', "l'affrontement super attendu en France c'est terminé par un coup innatendu, Cedric a marché sur une ECHARDE, l'arbitre a interommpu le combat car ce dernier c'est plaint.",1,  1),
('Bonne nuit Benoit','https://mmajunkie.usatoday.com/wp-content/uploads/sites/91/2024/03/dustin-poirier-benoit-saint-denis-ufc-299.jpg?w=1000&h=600&crop=1', "Le rookie français se prend un énorme ko et derrière se trouve l'excuse du staph, bonne nuit au revoir",1, 1),
('Salahdine Parnasse','https://www.actumma.com/wp-content/uploads/2023/03/salahdine-parnasse-double-champion-ksw.jpg', "Mais ou ira donc Parnasse après sa victoire flash au KSW Paris",2,  2),
('Islam Makhachev','https://www.palestinechronicle.com/wp-content/uploads/2023/10/Islam_Makhacev_Palestinian_Flag.png',"Champion des Lightweight",3, 1),
('Alex Peirera','https://sportshub.cbsistatic.com/i/r/2023/11/12/f4a80401-9936-439e-bc84-fecdf6300d99/thumbnail/1200x675/8f2ffa61d70b481d035793d562fa1bff/alex-pereira-champion.jpg', "Nous sommes à 4 jours de l'ufc 300, Pereira peut t'il défendre son titre et ainsi venger son mentor",3, 2);



INSERT INTO commentaires (name, date, content, id_articles) VALUES
('Commentaire 1', '2024-04-01','content 1',1),
('Commentaire 2', '2024-04-05','content 2',1),
('Commentaire 3', '2024-04-12','content 3',1),
('Commentaire 4', '2024-04-15','content 4',1);




-- SELECT * FROM commentaires



