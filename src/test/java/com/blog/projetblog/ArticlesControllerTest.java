package com.blog.projetblog;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.blog.projetblog.ProjetBlogApplication;


@SpringBootTest(classes = ProjetBlogApplication.class)
@AutoConfigureMockMvc
@Sql("/database.sql")
public class ArticlesControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testFindAll() throws Exception {
        mvc.perform(get("/api/article"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]['title']").exists())
                .andExpect(jsonPath("$[0]['title']").value("Titre 1"));
    }

    @Test
    void testAllOfCategorie() throws Exception {
        mvc.perform(get("/api/article/categorie/1"))
                .andExpect(status().isOk());
    }
}
